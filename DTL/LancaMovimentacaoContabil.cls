Class ACSC.Negocios.Contabil.Colegios.Totvs.IntegraFiscalMV.DTL.LancaMovimentacaoContabil Extends Ens.DataTransform
{

ClassMethod Transform(source As %ListOfObjects, target As ACSC.Negocios.Contabil.Colegios.Totvs.IntegraFiscalMV.VoSql.GravaMovimentacaoContabil) As %Status
{


	SET $ZT="Trap",tSC=$$$OK
	DO {
		SET source.ElementType = "ACSC.Negocios.Contabil.Colegios.Totvs.IntegraFiscalMV.VoSql.ConsultaMovimentoContabil"
	    ;Instanciado a variavel target
	    SET target = ##class(ACSC.Negocios.Contabil.Colegios.Totvs.IntegraFiscalMV.VoSql.GravaMovimentacaoContabil).%New()
        SET target.ObjMovimentoContabil				= ##Class(%ListOfObjects).%New()
        SET target.objMovimentoContabil.ElementType = "ACSC.Negocios.Contabil.Colegios.Totvs.IntegraFiscalMV.VoSql.GravaMovimentacaoContabil"
        FOR i=1:1:source.Count()
        {
        	;Referenciando a variavel tMovimentacaoContabilMV
        	#DIM tMovimentacaoContabilMV AS ACSC.Negocios.Contabil.Colegios.Totvs.IntegraFiscalMV.VoSql.ConsultaMovimentoContabil

            SET tMovimentacaoContabilMV							= source.GetAt(i)
            
            SET tMovContab                                      = ##class(ACSC.Negocios.Contabil.Colegios.Totvs.IntegraFiscalMV.VoSql.GravaMovimentacaoContabil).%New()
			
			SET $ZOBJPROPERTY(tMovContab,"pVCD_NUMERO")         		= tMovimentacaoContabilMV.Result.Idlancamento        
            SET $ZOBJPROPERTY(tMovContab,"pVCD_CREDITO")        		= tMovimentacaoContabilMV.Result.Credito
            SET $ZOBJPROPERTY(tMovContab,"pVCD_DEBITO")         		= tMovimentacaoContabilMV.Result.Debito

            IF (tMovimentacaoContabilMV.Result.ValorCredito = 0) {            
            	SET $ZOBJPROPERTY(tMovContab,"PVLR_LANCADO")        	= tMovimentacaoContabilMV.Result.ValorDebito
            } ELSE {
	            SET $ZOBJPROPERTY(tMovContab,"PVLR_LANCADO")        	= tMovimentacaoContabilMV.Result.ValorCredito
            }
            
            SET $ZOBJPROPERTY(tMovContab,"PVCD_HIST_PADRAO")    		= tMovimentacaoContabilMV.Result.Historico
            SET $ZOBJPROPERTY(tMovContab,"PVCD_SETOR_CRED")     		= tMovimentacaoContabilMV.Result.CcCredito
            SET $ZOBJPROPERTY(tMovContab,"PVCD_SETOR_DEB")      		= tMovimentacaoContabilMV.Result.CcDebito
            
            IF (tMovimentacaoContabilMV.Result.ValorCredito = 0) {            
                SET $ZOBJPROPERTY(tMovContab,"PVLR_LCTO_SETOR")     	= tMovimentacaoContabilMV.Result.ValorDebito
            } ELSE {
	            SET $ZOBJPROPERTY(tMovContab,"PVLR_LCTO_SETOR")     	= tMovimentacaoContabilMV.Result.ValorCredito 
            }
            
            SET $ZOBJPROPERTY(tMovContab,"PCDESCRICAO")         		= tMovimentacaoContabilMV.Result.Descricao
            SET $ZOBJPROPERTY(tMovContab,"PNCD_MULTI_EMPRESA_ORIGEM")	= tMovimentacaoContabilMV.Result.Codfilial
            SET $ZOBJPROPERTY(tMovContab,"PVDTMOVIMENTACAO")            = tMovimentacaoContabilMV.Result.Data
            SET $ZOBJPROPERTY(tMovContab,"PCD_AUXILIAR_DEB")            = null
            SET $ZOBJPROPERTY(tMovContab,"PCD_AUXILIAR_CRD")            = null
            SET $ZOBJPROPERTY(tMovContab,"VQtdRegistros")               = tMovimentacaoContabilMV.Result.QtdRegistros                           
        }
    } WHILE (0)

Exit
	QUIT tSC
Trap
	SET $ZT="",tSC=$$$EnsSystemError
	GOTO Exit
}

}
