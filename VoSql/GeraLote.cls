/// Processo autom�tio de gera��o de classes para consulta din�mica <br />
/// Criada em 2020-04-28 12:42:56 <br />
/// Quantidade de propriedades:2
Class ACSC.Negocios.Contabil.Colegios.Totvs.IntegraFiscalMV.VoSql.GeraLote Extends (%SerialObject, %XML.Adaptor) [ Inheritance = left, ProcedureBlock ]
{

Property Status As %String(MAXLEN = "") [ SqlFieldName = STATUS ];

Property Descerro As %String(MAXLEN = "") [ SqlFieldName = DESCERRO ];

/// Query para consulta <br />
/// Quantidade de par�metros:4
Parameter Query = "{CALL ENSEMBLE.PKG_MOVIMENTO_CONTABIL.PRC_GERA_LOTE(?,?,?,?)}";

/// Retorna ParmsArray para executar procedure 							<br />
/// IOType:   1 represents an input parameter. 							<br />
///           2 represents an input/output parameter. 					<br />
///           4 represents an output parameter. 						<br />
/// 																	<br />
///  SqlType: 1 represents SQL_CHAR                    					<br />
///           4 represents SQL_INTEGER									<br />
///           6 represents SQL_FLOAT									<br />
///           8 represents SQL_DOUBLE									<br />
///           12 represents SQL_VARCHAR									<br />
///           93 represents SQL_DATETIME								<br />
///  Mais informa��es pom ser encontradas no link abaixo				<br />
///  https://cedocs.intersystems.com/latest/csp/docbook/DocBook.UI.Page.cls?KEY=ESQL_adapter_methods_creating#ESQL_C247568
ClassMethod GetParamArrayProcedure(pDynParms As %DynamicObject, Output oParms) As %Status
{
	SET tSC = $System.Status.OK()
	TRY
	{
		SET oParms						= 4
		THROW:+pDynParms.amount'=2 ##class(%Exception.StatusException).CreateFromStatus($System.Status.Error(5001,"Quantidade de par�metros enviada n�o corresponde a esperada."))
		SET oParms(1)					= pDynParms.%Get(1)
		SET oParms(1,"IOType")			= 1
		SET oParms(1,"SqlType")			= 12

		SET oParms(2)					= pDynParms.%Get(2)

		SET oParms(3)					= ""
		SET oParms(3,"IOType")			= 4

		SET oParms(4)					= ""
		SET oParms(4,"IOType")			= 4

	}
	CATCH(e)
	{
		SET tSC = e.AsStatus()
	}
	QUIT tSC
}

Storage Default
{
<Data name="GeraLoteState">
<Value name="1">
<Value>Status</Value>
</Value>
<Value name="2">
<Value>Descerro</Value>
</Value>
</Data>
<State>GeraLoteState</State>
<StreamLocation>^ACSC.Negocios.ConA206.GeraLoteS</StreamLocation>
<Type>%Library.CacheSerialState</Type>
}

}
