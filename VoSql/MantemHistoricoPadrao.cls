/// Processo autom�tio de gera��o de classes para consulta din�mica <br />
/// Criada em 2020-02-27 13:41:06 <br />
/// Quantidade de propriedades:0
Class ACSC.Negocios.Contabil.Colegios.Totvs.IntegraFiscalMV.VoSql.MantemHistoricoPadrao Extends (%SerialObject, %XML.Adaptor) [ Inheritance = left, ProcedureBlock ]
{

/// Query para consulta <br />
/// Quantidade de par�metros:4
Parameter Query = "{CALL ENSEMBLE.INTEGRA_MV_MANTEM_HISTORICOPADRAO(?,?,?,?)}";

/// Retorna ParmsArray para executar procedure 							<br />
/// IOType:   1 represents an input parameter. 							<br />
///           2 represents an input/output parameter. 					<br />
///           4 represents an output parameter. 						<br />
/// 																	<br />
///  SqlType: 1 represents SQL_CHAR                    					<br />
///           4 represents SQL_INTEGER									<br />
///           6 represents SQL_FLOAT									<br />
///           8 represents SQL_DOUBLE									<br />
///           12 represents SQL_VARCHAR									<br />
///           93 represents SQL_DATETIME								<br />
///  Mais informa��es pom ser encontradas no link abaixo				<br />
///  https://cedocs.intersystems.com/latest/csp/docbook/DocBook.UI.Page.cls?KEY=ESQL_adapter_methods_creating#ESQL_C247568
ClassMethod GetParamArrayProcedure(pDynParms As %DynamicObject, Output oParms) As %Status
{
	SET tSC = $System.Status.OK()
	TRY
	{
		SET oParms						= 4
		THROW:+pDynParms.amount'=4 ##class(%Exception.StatusException).CreateFromStatus($System.Status.Error(5001,"Quantidade de par�metros enviada n�o corresponde a esperada."))
		SET oParms(1)					= pDynParms.%Get(1)
		SET oParms(1,"IOType")			= 1
		SET oParms(1,"SqlType")			= 12

		SET oParms(2)					= pDynParms.%Get(2)
		SET oParms(2,"SqlType")			= 4

		SET oParms(3)					= pDynParms.%Get(3)

		SET oParms(4)					= pDynParms.%Get(4)

	}
	CATCH(e)
	{
		SET tSC = e.AsStatus()
	}
	QUIT tSC
}

Storage Default
{
<StreamLocation>^ACSC.NegocA206.MantemHistoD764S</StreamLocation>
<Type>%Library.CacheSerialState</Type>
}

}
