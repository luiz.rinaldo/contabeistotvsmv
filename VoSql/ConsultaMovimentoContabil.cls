/// Processo autom�tio de gera��o de classes para consulta din�mica <br />
/// Criada em 2020-04-27 15:05:10 <br />
/// Quantidade de propriedades:12
Class ACSC.Negocios.Contabil.Colegios.Totvs.IntegraFiscalMV.VoSql.ConsultaMovimentoContabil Extends (%SerialObject, %XML.Adaptor) [ Inheritance = left, ProcedureBlock ]
{

Property Codcoligada As %String(MAXLEN = "") [ SqlFieldName = CODCOLIGADA ];

Property Codfilial As %String(MAXLEN = "") [ SqlFieldName = CODFILIAL ];

Property Idlancamento As %String(MAXLEN = "") [ SqlFieldName = IDLANCAMENTO ];

Property Data As %String(MAXLEN = "") [ SqlFieldName = DATA ];

Property Credito As %String(MAXLEN = "") [ SqlFieldName = CREDITO ];

Property Debito As %String(MAXLEN = "") [ SqlFieldName = DEBITO ];

Property ValorCredito As %String(MAXLEN = "") [ SqlFieldName = VALOR_CREDITO ];

Property Historico As %String(MAXLEN = "") [ SqlFieldName = HISTORICO ];

Property CcCredito As %String(MAXLEN = "") [ SqlFieldName = CC_CREDITO ];

Property CcDebito As %String(MAXLEN = "") [ SqlFieldName = CC_DEBITO ];

Property ValorDebito As %String(MAXLEN = "") [ SqlFieldName = VALOR_DEBITO ];

Property Descricao As %String(MAXLEN = "") [ SqlFieldName = DESCRICAO ];

/// Query para consulta <br />
/// Quantidade de par�metros:2
Parameter Query = "SELECT CODCOLIGADA,
	                      CODFILIAL,
	                      IDLANCAMENTO,
	                      DATA,
	                      CREDITO,
	                      DEBITO,
                          CASE
                            WHEN VALOR_CREDITO <> 0 THEN VALOR_CREDITO
                            ELSE VALOR_DEBITO
                          END VALOR_CREDITO,
	                      HISTORICO,
	                      CC_CREDITO,
	                      CC_DEBITO,
                          CASE
                            WHEN VALOR_DEBITO <> 0 THEN VALOR_DEBITO
                            ELSE VALOR_CREDITO
                          END VALOR_DEBITO,       
	                      DESCRICAO
				   FROM ENSEMBLE.VW_EXPORTA_INTEGRACAO_CONTABIL_MV 
				    WHERE CODFILIAL = ? AND DATA = CONVERT(VARCHAR,?,103) ORDER BY DATA";

Storage Default
{
<Data name="ConsultaMovimentoContabilState">
<Value name="1">
<Value>Codcoligada</Value>
</Value>
<Value name="2">
<Value>Codfilial</Value>
</Value>
<Value name="3">
<Value>Idlancamento</Value>
</Value>
<Value name="4">
<Value>Data</Value>
</Value>
<Value name="5">
<Value>Credito</Value>
</Value>
<Value name="6">
<Value>Debito</Value>
</Value>
<Value name="7">
<Value>ValorCredito</Value>
</Value>
<Value name="8">
<Value>Historico</Value>
</Value>
<Value name="9">
<Value>CcCredito</Value>
</Value>
<Value name="10">
<Value>CcDebito</Value>
</Value>
<Value name="11">
<Value>ValorDebito</Value>
</Value>
<Value name="12">
<Value>Descricao</Value>
</Value>
</Data>
<State>ConsultaMovimentoContabilState</State>
<StreamLocation>^ACSC.NegocA206.ConsultaMov659CS</StreamLocation>
<Type>%Library.CacheSerialState</Type>
}

}
