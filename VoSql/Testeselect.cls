/// Processo autom�tio de gera��o de classes para consulta din�mica <br />
/// Criada em 2020-04-23 15:19:20 <br />
/// Quantidade de propriedades:4
Class ACSC.Negocios.Contabil.Colegios.Totvs.IntegraFiscalMV.VoSql.Testeselect Extends (%SerialObject, %XML.Adaptor) [ Inheritance = left, ProcedureBlock ]
{

Property CdImpFcct As %String(MAXLEN = "") [ SqlFieldName = CD_IMP_FCCT ];

Property CdIdentificacao As %String(MAXLEN = "") [ SqlFieldName = CD_IDENTIFICACAO ];

Property CdReduzidoCredito As %String(MAXLEN = "") [ SqlFieldName = CD_REDUZIDO_CREDITO ];

Property CdReduzidoDebito As %String(MAXLEN = "") [ SqlFieldName = CD_REDUZIDO_DEBITO ];

/// Query para consulta <br />
/// Quantidade de par�metros:0
Parameter Query = "SELECT CD_IMP_FCCT,
						  CD_IDENTIFICACAO,
						  CD_REDUZIDO_CREDITO,
						  CD_REDUZIDO_DEBITO
				   FROM IMP_FCCT;";

Storage Default
{
<Data name="TesteselectState">
<Value name="1">
<Value>CdImpFcct</Value>
</Value>
<Value name="2">
<Value>CdIdentificacao</Value>
</Value>
<Value name="3">
<Value>CdReduzidoCredito</Value>
</Value>
<Value name="4">
<Value>CdReduzidoDebito</Value>
</Value>
</Data>
<State>TesteselectState</State>
<StreamLocation>^ACSC.NegociosA206.TesteselectS</StreamLocation>
<Type>%Library.CacheSerialState</Type>
}

}
