/// Processo autom�tio de gera��o de classes para consulta din�mica <br />
/// Criada em 2020-03-05 17:28:48 <br />
/// Quantidade de propriedades:0
Class ACSC.Negocios.Contabil.Colegios.Totvs.IntegraFiscalMV.VoSql.MantemContaContabil Extends (%SerialObject, %XML.Adaptor) [ Inheritance = left, ProcedureBlock ]
{

/// Query para consulta <br />
/// Quantidade de par�metros:7
Parameter Query = "{CALL ENSEMBLE.INTEGRA_MV_MANTEM_CONTACONTABIL(?,?,?,?,?,?,?)}";

/// Retorna ParmsArray para executar procedure 							<br />
/// IOType:   1 represents an input parameter. 							<br />
///           2 represents an input/output parameter. 					<br />
///           4 represents an output parameter. 						<br />
/// 																	<br />
///  SqlType: 1 represents SQL_CHAR                    					<br />
///           4 represents SQL_INTEGER									<br />
///           6 represents SQL_FLOAT									<br />
///           8 represents SQL_DOUBLE									<br />
///           12 represents SQL_VARCHAR									<br />
///           93 represents SQL_DATETIME								<br />
///  Mais informa��es pom ser encontradas no link abaixo				<br />
///  https://cedocs.intersystems.com/latest/csp/docbook/DocBook.UI.Page.cls?KEY=ESQL_adapter_methods_creating#ESQL_C247568
ClassMethod GetParamArrayProcedure(pDynParms As %DynamicObject, Output oParms) As %Status
{
	SET tSC = $System.Status.OK()
	TRY
	{
		SET oParms						= 7
		THROW:+pDynParms.amount'=7 ##class(%Exception.StatusException).CreateFromStatus($System.Status.Error(5001,"Quantidade de par�metros enviada n�o corresponde a esperada."))
		SET oParms(1)					= pDynParms.%Get(1)
		SET oParms(1,"IOType")			= 1
		SET oParms(1,"SqlType")			= 12

		SET oParms(2)					= pDynParms.%Get(2)
		SET oParms(2,"SqlType")			= 4

		SET oParms(3)					= pDynParms.%Get(3)

		SET oParms(4)					= pDynParms.%Get(4)

		SET oParms(5)					= pDynParms.%Get(5)
		SET oParms(5,"SqlType")			= 4

		SET oParms(6)					= pDynParms.%Get(6)

		SET oParms(7)					= pDynParms.%Get(7)
		SET oParms(7,"SqlType")			= 4

	}
	CATCH(e)
	{
		SET tSC = e.AsStatus()
	}
	QUIT tSC
}

Storage Default
{
<StreamLocation>^ACSC.NegocA206.MantemContaB766S</StreamLocation>
<Type>%Library.CacheSerialState</Type>
}

}
