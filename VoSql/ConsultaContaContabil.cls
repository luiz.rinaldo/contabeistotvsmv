/// Processo autom�tio de gera��o de classes para consulta din�mica <br />
/// Criada em 2020-03-05 17:02:31 <br />
/// Quantidade de propriedades:6
Class ACSC.Negocios.Contabil.Colegios.Totvs.IntegraFiscalMV.VoSql.ConsultaContaContabil Extends (%SerialObject, %XML.Adaptor) [ Inheritance = left, ProcedureBlock ]
{

Property CdContabil As %String(MAXLEN = "") [ SqlFieldName = CD_CONTABIL ];

Property CdReduzido As %String(MAXLEN = "") [ SqlFieldName = CD_REDUZIDO ];

Property DsConta As %String(MAXLEN = "") [ SqlFieldName = DS_CONTA ];

Property Natureza As %String(MAXLEN = "") [ SqlFieldName = NATUREZA ];

Property Natsped As %String(MAXLEN = "") [ SqlFieldName = NATSPED ];

Property Inativo As %String(MAXLEN = "") [ SqlFieldName = INATIVO ];

/// Query para consulta <br />
/// Quantidade de par�metros:1
Parameter Query = "SELECT PC.CD_CONTABIL,
						  PC.CD_REDUZIDO,
						  PC.DS_CONTA,
						  (SELECT CASE CC.TP_NATUREZA WHEN 'D' THEN 1 ELSE 0 END TP_NATUREZA FROM CONFIG_CONTABIL CC WHERE CC.CD_CONTABIL = SUBSTR(PC.CD_CONTABIL,1,1) AND CC.CD_MULTI_EMPRESA = 1) NATUREZA,
						  '0' || SUBSTR(PC.CD_CONTABIL,1,1) NATSPED,
						  (CASE SN_ATIVO WHEN 'S' THEN 1 ELSE 0 END) INATIVO
				   FROM DBAMV.PLANO_CONTAS PC 				    WHERE PC.CD_REDUZIDO = ?";

Storage Default
{
<Data name="ConsultaContaContabilState">
<Value name="1">
<Value>CdContabil</Value>
</Value>
<Value name="2">
<Value>CdReduzido</Value>
</Value>
<Value name="3">
<Value>DsConta</Value>
</Value>
<Value name="4">
<Value>Natureza</Value>
</Value>
<Value name="5">
<Value>Natsped</Value>
</Value>
<Value name="6">
<Value>Inativo</Value>
</Value>
</Data>
<State>ConsultaContaContabilState</State>
<StreamLocation>^ACSC.NegocA206.ConsultaCon1501S</StreamLocation>
<Type>%Library.CacheSerialState</Type>
}

}
