/// Processo autom�tio de gera��o de classes para consulta din�mica <br />
/// Criada em 2020-04-15 11:54:03 <br />
/// Quantidade de propriedades:2
Class ACSC.Negocios.Contabil.Colegios.Totvs.IntegraFiscalMV.VoSql.GravaMovimentacaoContabil Extends (%SerialObject, %XML.Adaptor) [ Inheritance = left, ProcedureBlock ]
{

Property Descricaoerro As %String(MAXLEN = "") [ SqlFieldName = DESCRICAOERRO ];

Property Status As %String(MAXLEN = "") [ SqlFieldName = STATUS ];

/// Query para consulta <br />
/// Quantidade de par�metros:14
Parameter Query = "{CALL ENSEMBLE.PKG_MOVIMENTO_CONTABIL.PRC_GRV_MOV_CONTAB(?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";

/// Retorna ParmsArray para executar procedure 							<br />
/// IOType:   1 represents an input parameter. 							<br />
///           2 represents an input/output parameter. 					<br />
///           4 represents an output parameter. 						<br />
/// 																	<br />
///  SqlType: 1 represents SQL_CHAR                    					<br />
///           4 represents SQL_INTEGER									<br />
///           6 represents SQL_FLOAT									<br />
///           8 represents SQL_DOUBLE									<br />
///           12 represents SQL_VARCHAR									<br />
///           93 represents SQL_DATETIME								<br />
///  Mais informa��es pom ser encontradas no link abaixo				<br />
///  https://cedocs.intersystems.com/latest/csp/docbook/DocBook.UI.Page.cls?KEY=ESQL_adapter_methods_creating#ESQL_C247568
ClassMethod GetParamArrayProcedure(pDynParms As %DynamicObject, Output oParms) As %Status
{
	SET tSC = $System.Status.OK()
	TRY
	{
		SET oParms						= 14
		THROW:+pDynParms.amount'=12 ##class(%Exception.StatusException).CreateFromStatus($System.Status.Error(5001,"Quantidade de par�metros enviada n�o corresponde a esperada."))
		SET oParms(1)					= pDynParms.%Get(1)
		SET oParms(1,"IOType")			= 1
		SET oParms(1,"SqlType")			= 12

		SET oParms(2)					= pDynParms.%Get(2)

		SET oParms(3)					= pDynParms.%Get(3)

		SET oParms(4)					= pDynParms.%Get(4)

		SET oParms(5)					= pDynParms.%Get(5)

		SET oParms(6)					= pDynParms.%Get(6)

		SET oParms(7)					= pDynParms.%Get(7)

		SET oParms(8)					= pDynParms.%Get(8)

		SET oParms(9)					= pDynParms.%Get(9)

		SET oParms(10)					= pDynParms.%Get(10)

		SET oParms(11)					= pDynParms.%Get(11)

		SET oParms(12)					= pDynParms.%Get(12)
		SET oParms(12,"SqlType")		= 4

		SET oParms(13)					= ""
		SET oParms(13,"IOType")			= 4

		SET oParms(14)					= ""
		SET oParms(14,"IOType")			= 4

	}
	CATCH(e)
	{
		SET tSC = e.AsStatus()
	}
	QUIT tSC
}

Storage Default
{
<Data name="GravaMovimentacaoContabilState">
<Value name="1">
<Value>Descricaoerro</Value>
</Value>
<Value name="2">
<Value>Status</Value>
</Value>
</Data>
<State>GravaMovimentacaoContabilState</State>
<StreamLocation>^ACSC.NegocA206.GravaMovime1A15S</StreamLocation>
<Type>%Library.CacheSerialState</Type>
}

}
