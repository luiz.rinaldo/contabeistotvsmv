/// Processo autom�tio de gera��o de classes para consulta din�mica <br />
/// Criada em 2020-02-21 16:06:56 <br />
/// Quantidade de propriedades:1
Class ACSC.Negocios.Contabil.Colegios.Totvs.IntegraFiscalMV.VoSql.ConsultaHistorico Extends (%SerialObject, %XML.Adaptor) [ Inheritance = left, ProcedureBlock ]
{

Property DsHistoricoPadrao As %String(MAXLEN = "") [ SqlFieldName = DS_HISTORICO_PADRAO ];

/// Query para consulta <br />
/// Quantidade de par�metros:1
Parameter Query = "SELECT DS_HISTORICO_PADRAO
				   FROM DBAMV.HISTORICO_PADRAO 				    WHERE CD_HISTORICO_PADRAO = ?";

Storage Default
{
<Data name="ConsultaHistoricoState">
<Value name="1">
<Value>DsHistoricoPadrao</Value>
</Value>
</Data>
<State>ConsultaHistoricoState</State>
<StreamLocation>^ACSC.NegocA206.ConsultaHisCB51S</StreamLocation>
<Type>%Library.CacheSerialState</Type>
}

}
