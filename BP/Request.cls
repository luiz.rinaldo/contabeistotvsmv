Class ACSC.Negocios.Contabil.Colegios.Totvs.IntegraFiscalMV.BP.Request Extends (Ens.Request, ACSC.Utils.ExpurgoPropriedadesPersistentes, %XML.Adaptor)
{

Parameter XMLSEQUENCE = 0;

Parameter XMLNAME = "Mensagem";

Property XML As %Stream.GlobalCharacter;

Property Metodo As %String;

Property TipoSolicita As %String(MAXLEN = "");

Property TipoIntegracao As %String(MAXLEN = "");

Property codcoligada As %String(MAXLEN = "");

Property codigo As %String(MAXLEN = "");

Property codconta As %String(MAXLEN = "");

Property codhistp As %String(MAXLEN = "");

Property descricao As %String(MAXLEN = "");

Property reduzido As %String(MAXLEN = "");

Property natureza As %String(MAXLEN = "");

Property natSped As %String(MAXLEN = "");

Property inativa As %String(MAXLEN = "");

Property DataHoraEnvio As %String(MAXLEN = "");

Property Competencia As %String(MAXLEN = "");

Property CodigoEmpresa As %String(MAXLEN = "");

Property Input As %Stream.GlobalCharacter;

Storage Default
{
<Data name="RequestDefaultData">
<Subscript>"Request"</Subscript>
<Value name="1">
<Value>XML</Value>
</Value>
<Value name="2">
<Value>Metodo</Value>
</Value>
<Value name="3">
<Value>TipoSolicita</Value>
</Value>
<Value name="4">
<Value>TipoIntegracao</Value>
</Value>
<Value name="5">
<Value>codcoligada</Value>
</Value>
<Value name="6">
<Value>codigo</Value>
</Value>
<Value name="7">
<Value>codconta</Value>
</Value>
<Value name="8">
<Value>codhistp</Value>
</Value>
<Value name="9">
<Value>descricao</Value>
</Value>
<Value name="10">
<Value>reduzido</Value>
</Value>
<Value name="11">
<Value>natureza</Value>
</Value>
<Value name="12">
<Value>natSped</Value>
</Value>
<Value name="13">
<Value>inativa</Value>
</Value>
<Value name="14">
<Value>DataHoraEnvio</Value>
</Value>
<Value name="15">
<Value>Competencia</Value>
</Value>
<Value name="16">
<Value>CodigoEmpresa</Value>
</Value>
<Value name="17">
<Value>Input</Value>
</Value>
</Data>
<DefaultData>RequestDefaultData</DefaultData>
<Type>%Library.CacheStorage</Type>
}

}
