/// 
Class ACSC.Negocios.Contabil.Colegios.Totvs.IntegraFiscalMV.BP.Process Extends Ens.BusinessProcessBPL
{

/// BPL Definition
XData BPL [ XMLNamespace = "http://www.intersystems.com/bpl" ]
{
<process language='objectscript' request='ACSC.Negocios.Contabil.Colegios.Totvs.IntegraFiscalMV.BP.Request' response='ACSC.Negocios.Contabil.Colegios.Totvs.IntegraFiscalMV.BP.Response' height='2000' width='2000' >
<context>
<property name='ObjDescricao' type='ACSC.Negocios.Contabil.Colegios.Totvs.IntegraFiscalMV.VoSql.ConsultaHistorico' instantiate='0' />
<property name='ObjContaContabil' type='ACSC.Negocios.Contabil.Colegios.Totvs.IntegraFiscalMV.VoSql.ConsultaContaContabil' instantiate='0' />
<property name='ObjMovimentoContabil' type='ACSC.Negocios.Contabil.Colegios.Totvs.IntegraFiscalMV.VoSql.ConsultaMovimentoContabil' instantiate='0' />
<property name='i' type='%Integer' instantiate='0' />
<property name='StatusRetorno' type='%String' initialexpression='""' instantiate='0' >
<parameters>
<parameter name='MAXLEN'  value='400' />
</parameters>
</property>
<property name='ListaMovimentoContabil' type='ACSC.Negocios.Contabil.Colegios.Totvs.IntegraFiscalMV.VoSql.ConsultaMovimentoContabil' initialexpression='""' collection='list' instantiate='0' />
<property name='RespObj' type='%RegisteredObject' instantiate='0' />
<property name='GravaMovimentacaoContabil' type='ACSC.Negocios.Contabil.Colegios.Totvs.IntegraFiscalMV.VoSql.GravaMovimentacaoContabil' instantiate='0' />
<property name='DtIniMC' type='%Date' instantiate='0' >
<annotation><![CDATA[Data inicial da movimentação contábil]]></annotation>
</property>
<property name='DtFimMC' type='%Date' instantiate='0' >
<annotation><![CDATA[Data final da movimentação contábil]]></annotation>
</property>
<property name='GeraLote' type='ACSC.Negocios.Contabil.Colegios.Totvs.IntegraFiscalMV.VoSql.GeraLote' instantiate='0' />
</context>
<sequence xend='200' yend='800' >
<switch xpos='200' ypos='250' xend='200' yend='700' >
<case condition='(request.TipoIntegracao="MOVIMENTACAO_CONTABIL")' name='Movimentação Contábil SP' >
<sequence name='Movimentação Contábil' xpos='335' ypos='400' xend='200' yend='550' >
<assign name="Get Data inicial" property="context.DtIniMC" value="$ZDH(request.Competencia,3)" action="set" xpos='200' ypos='250' />
<assign name="Get Data Final" property="context.DtFimMC" value="$ZDH($System.SQL.DATEADD(&quot;MM&quot;,1,request.Competencia),3)" action="set" xpos='200' ypos='350' />
<while name='Envia dia a dia' condition='context.DtIniMC&lt;context.DtFimMC' xpos='200' ypos='450' xend='200' yend='900' >
<switch name='ChecandoEmpresa' xpos='200' ypos='250' xend='200' yend='500' >
<case condition='request.CodigoEmpresa="8"' name='ColegioSP' >
<call name='ConsultaMovimentacaoContabilSP' target='Totvs SP SQL Dynamic Connnector' async='0' xpos='335' ypos='400' >
<request type='ACSC.SQL.DynamicQuery.GetJSONFromQueryReq' >
<assign property="callrequest.VoClass" value="&quot;ACSC.Negocios.Contabil.Colegios.Totvs.IntegraFiscalMV.VoSql.ConsultaMovimentoContabil&quot;" action="set" />
<assign property="callrequest.ParmsJson" value="##class(ACSC.SQL.DynamicQuery.Util).GetParmsJson(request.CodigoEmpresa,$ZD(context.DtIniMC,3))" action="set" />
</request>
<response type='ACSC.SQL.DynamicQuery.Response' >
<assign property="context.ListaMovimentoContabil" value="##Class(ACSC.SQL.DynamicQuery.Util).SqlDinJSONStreamToObject(callresponse.Result,callresponse.VoClass,0)" action="set" />
</response>
</call>
</case>
<case condition='request.CodigoEmpresa="6"' name='ColegioJF' >
<call name='ConsultaMovimentacaoContabilJF' target='Totvs MG SQL Dynamic Connector' async='0' xpos='605' ypos='400' >
<request type='ACSC.SQL.DynamicQuery.GetJSONFromQueryReq' >
<assign property="callrequest.VoClass" value="&quot;ACSC.Negocios.Contabil.Colegios.Totvs.IntegraFiscalMV.VoSql.ConsultaMovimentoContabil&quot;" action="set" />
<assign property="callrequest.ParmsJson" value="##class(ACSC.SQL.DynamicQuery.Util).GetParmsJson(request.CodigoEmpresa,$ZD(context.DtIniMC,3))" action="set" />
</request>
<response type='ACSC.SQL.DynamicQuery.Response' >
<assign property="context.ListaMovimentoContabil" value="##Class(ACSC.SQL.DynamicQuery.Util).SqlDinJSONStreamToObject(callresponse.Result,callresponse.VoClass,0)" action="set" />
</response>
</call>
</case>
<default/>
</switch>
<trace name='Trace' value='"Competencia do dia:"_$ZD(context.DtIniMC,3)_" || QTD:"_context.ListaMovimentoContabil.Count()' xpos='200' ypos='600' >
<annotation><![CDATA[Cria trace do dia da competência e quantidade de registros]]></annotation>
</trace>
<foreach name='Gravando Movimento Contabil' property='context.ListaMovimentoContabil' key='context.i' xpos='200' ypos='700' xend='200' yend='450' >
<assign name="Objeto Movimento Contabil" property="context.ObjMovimentoContabil" value="context.ListaMovimentoContabil.GetAt(context.i)" action="set" xpos='200' ypos='250' />
<call name='Grava Movimento Contabil' target='MVSOUL CORP SQL Dynamic Connector' async='0' xpos='200' ypos='350' >
<request type='ACSC.SQL.DynamicQuery.GetJSONFromQueryReq' >
<assign property="callrequest.VoClass" value="&quot;ACSC.Negocios.Contabil.Colegios.Totvs.IntegraFiscalMV.VoSql.GravaMovimentacaoContabil&quot;" action="set" />
<assign property="callrequest.ParmsJson" value="##class(ACSC.SQL.DynamicQuery.Util).GetParmsJson(context.ObjMovimentoContabil.Idlancamento,context.ObjMovimentoContabil.Credito,context.ObjMovimentoContabil.Debito,context.ObjMovimentoContabil.ValorCredito,context.ObjMovimentoContabil.Historico,context.ObjMovimentoContabil.CcCredito,context.ObjMovimentoContabil.CcDebito,context.ObjMovimentoContabil.ValorDebito,context.ObjMovimentoContabil.Descricao,context.ObjMovimentoContabil.Codfilial,context.ObjMovimentoContabil.Data,context.ListaMovimentoContabil.Count())" action="set" />
</request>
<response type='ACSC.SQL.DynamicQuery.Response' >
<assign property="context.GravaMovimentacaoContabil" value="##Class(ACSC.SQL.DynamicQuery.Util).SqlDinJSONStreamToObject(callresponse.Result,callresponse.VoClass,1)" action="set" />
<assign property="response.DescricaoErro" value="context.GravaMovimentacaoContabil.Descricaoerro" action="set" />
<assign property="response.Status" value="context.GravaMovimentacaoContabil.Status" action="set" />
</response>
</call>
</foreach>
<assign name="Incrementa um dia" property="context.DtIniMC" value="context.DtIniMC+1" action="set" xpos='200' ypos='800' />
</while>
</sequence>
<call name='Gera Lote' target='MVSOUL CORP SQL Dynamic Connector' async='0' xpos='335' ypos='500' >
<request type='ACSC.SQL.DynamicQuery.GetJSONFromQueryReq' >
<assign property="callrequest.VoClass" value="&quot;ACSC.Negocios.Contabil.Colegios.Totvs.IntegraFiscalMV.VoSql.GeraLote&quot;" action="set" />
<assign property="callrequest.ParmsJson" value="##class(ACSC.SQL.DynamicQuery.Util).GetParmsJson(request.CodigoEmpresa, request.Competencia)" action="set" />
</request>
<response type='ACSC.SQL.DynamicQuery.Response' >
<assign property="context.GeraLote" value="##Class(ACSC.SQL.DynamicQuery.Util).SqlDinJSONStreamToObject(callresponse.Result,callresponse.VoClass,1)" action="set" />
<assign property="response.DescricaoErro" value="context.GeraLote.Descerro" action="set" />
<assign property="response.Status" value="context.GeraLote.Status" action="set" />
</response>
</call>
</case>
<case condition='(request.TipoIntegracao="CONTA_CONTABIL_MV")' name='ContaContabil SP' >
<call name='ConsultaContaContabil' target='MVSOUL CORP SQL Dynamic Connector' async='0' xpos='605' ypos='400' >
<request type='ACSC.SQL.DynamicQuery.GetJSONFromQueryReq' >
<assign property="callrequest.VoClass" value="&quot;ACSC.Negocios.Contabil.Colegios.Totvs.IntegraFiscalMV.VoSql.ConsultaContaContabil&quot;" action="set" />
<assign property="callrequest.ParmsJson" value="##class(ACSC.SQL.DynamicQuery.Util).GetParmsJson(request.codigo)" action="set" />
</request>
<response type='ACSC.SQL.DynamicQuery.Response' >
<assign property="context.ObjContaContabil" value="##Class(ACSC.SQL.DynamicQuery.Util).SqlDinJSONStreamToObject(callresponse.Result,callresponse.VoClass)" action="set" />
</response>
</call>
<call name='GrvContaContabTotvsMG' target='Totvs SP SQL Dynamic Connnector' async='0' xpos='605' ypos='500' >
<request type='ACSC.SQL.DynamicQuery.GetJSONFromQueryReq' >
<assign property="callrequest.VoClass" value="&quot;ACSC.Negocios.Contabil.Colegios.Totvs.IntegraFiscalMV.VoSql.MantemContaContabil&quot;" action="set" />
<assign property="callrequest.ParmsJson" value="##class(ACSC.SQL.DynamicQuery.Util).GetParmsJson(request.TipoSolicita, request.codcoligada, context.ObjContaContabil.CdContabil, context.ObjContaContabil.DsConta, context.ObjContaContabil.Natureza, context.ObjContaContabil.Natsped, context.ObjContaContabil.Inativo)" action="set" />
</request>
<response type='ACSC.SQL.DynamicQuery.Response' />
</call>
<call name='GrvContaContabTotvsSP' target='Totvs SP SQL Dynamic Connnector' async='0' xpos='605' ypos='600' >
<request type='ACSC.SQL.DynamicQuery.GetJSONFromQueryReq' >
<assign property="callrequest.VoClass" value="&quot;ACSC.Negocios.Contabil.Colegios.Totvs.IntegraFiscalMV.VoSql.MantemContaContabil&quot;" action="set" />
<assign property="callrequest.ParmsJson" value="##class(ACSC.SQL.DynamicQuery.Util).GetParmsJson(request.TipoSolicita, request.codcoligada, context.ObjContaContabil.CdContabil, context.ObjContaContabil.DsConta, context.ObjContaContabil.Natureza, context.ObjContaContabil.Natsped, context.ObjContaContabil.Inativo)" action="set" />
</request>
<response type='ACSC.SQL.DynamicQuery.Response' />
</call>
</case>
<case condition='(request.TipoIntegracao="HISTORICO_PADRAO_MV")' name='Histórico' >
<call name='ConsultaHistorico' target='MVSOUL CORP SQL Dynamic Connector' async='0' xpos='875' ypos='400' >
<request type='ACSC.SQL.DynamicQuery.GetJSONFromQueryReq' >
<assign property="callrequest.VoClass" value="&quot;ACSC.Negocios.Contabil.Colegios.Totvs.IntegraFiscalMV.VoSql.ConsultaHistorico&quot;" action="set" />
<assign property="callrequest.ParmsJson" value="##class(ACSC.SQL.DynamicQuery.Util).GetParmsJson(request.codigo)" action="set" />
</request>
<response type='ACSC.SQL.DynamicQuery.Response' >
<assign property="context.ObjDescricao" value="##Class(ACSC.SQL.DynamicQuery.Util).SqlDinJSONStreamToObject(callresponse.Result,callresponse.VoClass)" action="set" />
</response>
</call>
<call name='GrvHistoricoTotvsMG' target='Totvs MG SQL Dynamic Connector' async='0' xpos='875' ypos='500' >
<request type='ACSC.SQL.DynamicQuery.GetJSONFromQueryReq' >
<assign property="callrequest.VoClass" value="&quot;ACSC.Negocios.Contabil.Colegios.Totvs.IntegraFiscalMV.VoSql.MantemHistoricoPadrao&quot;" action="set" />
<assign property="callrequest.ParmsJson" value="##class(ACSC.SQL.DynamicQuery.Util).GetParmsJson(request.TipoSolicita,request.codcoligada, request.codigo, context.ObjDescricao.DsHistoricoPadrao)" action="set" />
</request>
<response type='ACSC.SQL.DynamicQuery.Response' />
</call>
<call name='GrvHistoricoTotvsSP' target='Totvs SP SQL Dynamic Connnector' async='0' xpos='875' ypos='600' >
<request type='ACSC.SQL.DynamicQuery.GetJSONFromQueryReq' >
<assign property="callrequest.VoClass" value="&quot;ACSC.Negocios.Contabil.Colegios.Totvs.IntegraFiscalMV.VoSql.MantemHistoricoPadrao&quot;" action="set" />
<assign property="callrequest.ParmsJson" value="##class(ACSC.SQL.DynamicQuery.Util).GetParmsJson(request.TipoSolicita,request.codcoligada, request.codigo, context.ObjDescricao.DsHistoricoPadrao)" action="set" />
</request>
<response type='ACSC.SQL.DynamicQuery.Response' />
</call>
</case>
<default name='default' />
</switch>
</sequence>
</process>
}

Storage Default
{
<Type>%Library.CacheStorage</Type>
}

}
