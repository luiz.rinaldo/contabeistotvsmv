Class ACSC.Negocios.Contabil.Colegios.Totvs.IntegraFiscalMV.BP.Response Extends Ens.Response
{

Property Status As %String(MAXLEN = "");

Property DescricaoErro As %String(MAXLEN = "");

Storage Default
{
<Data name="ResponseDefaultData">
<Subscript>"Response"</Subscript>
<Value name="1">
<Value>Status</Value>
</Value>
<Value name="2">
<Value>DescricaoErro</Value>
</Value>
</Data>
<DefaultData>ResponseDefaultData</DefaultData>
<Type>%Library.CacheStorage</Type>
}

}
