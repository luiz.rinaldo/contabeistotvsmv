Class ACSC.Negocios.Contabil.Colegios.Totvs.IntegraFiscalMV.BS.Service Extends Ens.BusinessService
{

Parameter ADAPTER = "EnsLib.HTTP.InboundAdapter";

Property Adapter As EnsLib.HTTP.InboundAdapter;

Method OnProcessInput(pInput As %Stream.Object, Output pOutput As %Stream.Object) As %Status
{
	
	SET tSC = $System.Status.OK()
	TRY
	{	
	    SET pOutput 		= ##class(%GlobalCharacterStream).%New()
		SET tHttpRequest 	= $ZCONVERT(pInput.Attributes("HttpRequest"),"U")
		
		IF (tHttpRequest '= "OPTIONS")	
		{
			SET tUrl      	= pInput.Attributes("URL")			
			SET tMethod    	= $ZCONVERT($PIECE(tUrl,"/",2),"U")
			IF tHttpRequest'="POST"
			{
				DO pOutput.Write($ZCONVERT("<resultado><status>erro</status><descricao>Requisi��o n�o aceita tipo '"_pInput.Attributes("HttpRequest")_"'</descricao></resultado>","O","UTF8"))	
			}
			ELSEIF tMethod="PROCESSA"
			{				
				SET tSC = ..Processa(pInput,.pOutput)				
			}	
			ELSE
			{
				DO pOutput.Write($ZCONVERT("<resultado><status>erro</status><descricao>M�todo '"_$PIECE(tUrl,"/",2)_"' desconhecido</descricao></resultado>","O","UTF8"))
			}	
			
		}	
		
		SET pOutput.Attributes("Content-Type")="application/json; charset=utf-8"	
		SET pOutput.Attributes("Access-Control-Allow-Origin") = "*"
		SET pOutput.Attributes("Access-Control-Allow-Methods")="GET, PUT, POST, DELETE, OPTIONS"
 		SET pOutput.Attributes("Access-Control-Allow-Headers")="Content-Type, Content-Range, Content-Disposition, Content-Description"	
		
	}
	CATCH (e)
	{
		SET tSC = e.AsStatus()
	}    
	QUIT tSC
}

Method Processa(pInput As %Stream.GlobalCharacter, ByRef pOutput) As %Status
{
	TRY
	{
		SET tUrl 			= pInput.Attributes("URL")	
		SET tXML 			= $ZCONVERT($E(tUrl,$F($ZCONVERT(tUrl,"U"),"PROCESSA/"),$L(tUrl)),"I","URL")
		
		SET tReader 		= ##class(%XML.Reader).%New()
		SET tRequest 		= ##Class(ACSC.Negocios.Contabil.Colegios.Totvs.IntegraFiscalMV.BP.Request).%New()
        
        #DIM tResponse As ACSC.Negocios.Contabil.Colegios.Totvs.IntegraFiscalMV.BP.Response		
		
		SET tSC 			= tReader.OpenString(tXML)
		DO tReader.Rewind()	
		DO tReader.Correlate("Mensagem","ACSC.Negocios.Contabil.Colegios.Totvs.IntegraFiscalMV.BP.Request")
		DO tReader.Next(.tRequest,.tSC)			
		THROW:$$$ISERR(tSC) ##Class(%Exception.StatusException).CreateFromStatus(tSC)

		
		SET tRequest.Input 	= ##Class(%Stream.GlobalCharacter).%New()
		DO tRequest.Input.Write(tXML)
		SET tSC 			= ..SendRequestAsync("Envia Dados Contab Colegios",tRequest,.tResponse)
		;SET tStatus		= tResponse.Status
		SET tStatus         = "Processando !"
		;SET tDescErro 		= tResponse.DescricaoErro
		SET tDescErro 		= ""
				
		
	}
	CATCH(e)
	{
		SET tSC 		= e.AsStatus()
		SET tDescErro	= e.Data
		SET tStatus		= "erro"
	}
	DO pOutput.Write("<resultado><status>"_tStatus_"</status>")
	DO:tDescErro'="" pOutput.Write("<descricao>"_tDescErro_"</descricao>")
	DO pOutput.Write("</resultado>")
	QUIT tSC
}

}
